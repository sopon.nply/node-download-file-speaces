# Downloads File Spaces for Node.js

## 🌟 Features

- download file
- download file all for spaces

## 🚀 Installation

```bash
npm install --save
```

## Setup ENV
Please fill out the information completely.
```bash
SPACES_ACCESS_KEY_ID=
SPACES_SECRET_ACCESS_KEY=
END_POINT=https://sgp1.digitaloceanspaces.com
BUCKET_NAME=
```

## License

MIT