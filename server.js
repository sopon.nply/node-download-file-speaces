const fs = require("fs");
const path = require('path');
const { config } = require('./configs');
const { S3Client, ListObjectsV2Command, GetObjectCommand } = require("@aws-sdk/client-s3");
const { pipeline } = require('stream');
const { promisify } = require('util');

const streamPipeline = promisify(pipeline);
const s3Client = new S3Client({
    region: "sgp1",
    endpoint: config.END_POINT,
    credentials: {
        accessKeyId: config.SPACES_ACCESS_KEY_ID,
        secretAccessKey: config.SPACES_SECRET_ACCESS_KEY,
    },
});

const listFiles = async (bucketName) => {
    const params = {
        Bucket: bucketName
    };
    try {
        const command = new ListObjectsV2Command(params);
        const data = await s3Client.send(command);
        return data.Contents || [];
    } catch (error) {
        console.error('Error listing files:', error.message);
        throw error;
    }
};

const downloadFile = async (bucketName, fileKey, downloadDir) => {
    const params = {
        Bucket: bucketName,
        Key: fileKey
    };
    try {
        const command = new GetObjectCommand(params);
        const data = await s3Client.send(command);
        const filePath = path.join(downloadDir, fileKey);
        fs.mkdirSync(path.dirname(filePath), { recursive: true });
        const writeStream = fs.createWriteStream(filePath);
        await streamPipeline(data.Body, writeStream);
        console.log(`File downloaded successfully to ${filePath}`);
    } catch (error) {
        console.error(`Error downloading file ${fileKey}:`, error.message);
    }
};

const downloadAllFiles = async (bucketName, downloadDir) => {
    try {
        const files = await listFiles(bucketName);
        for (const file of files) {
            await downloadFile(bucketName, file.Key, downloadDir);
        }
    } catch (error) {
        console.error('Error downloading files:', error.message);
    }
};

// Usage example
const bucketName = config.BUCKET_NAME;
const downloadDir = path.join(__dirname, 'downloaded-files');

downloadAllFiles(bucketName, downloadDir);